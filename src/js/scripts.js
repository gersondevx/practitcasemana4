$(document).ready(function(){

    $("#myForm").submit(function(evt){
        evt.preventDefault();
    });

    $("#enviarDatos").click(function(){
        swal({
            title: "Gracias :)",
            text: "Su solicitud sera atendida lo antes posible",
            icon: "success"
        });
    });

    $("#btnIniciarSesion").click(function(){
        console.log("Abriendo formulario.")
    });

    $("#btnEnviar").click(function(){
        console.log("Datos enviados con exito");
    });

    $("#btnCerrar").click(function(){
        console.log("Se cerró el formulario");
    });
});
